//----------------------Customer.h--------------------------------
// -
// Matthew Stibbins CS 343
// 3/14/2018
// ---------------------------------------------------------------
//
//

#include "Customer.h"

// ostream overload. prints out the string representation of customer
ostream& operator<<(ostream& out, Customer& m) {
    out << m.toString() << endl;
    return (out);
}


// print customers history of transactions in 
// chronological order : Most recent listed first
void Customer::printHistory() const
{
	if(history->size()==0){
		return;
	}
    cout << endl << endl;
    cout << "Customer with ID : " << setw(5) << id << "   : Transaction History" << endl << "--------------------------------------------------" << endl;

    cout << setw(6) << "Action" <<setw(22) << "|" << setw(18) << "Movie" << endl;
    cout << "--------------------------------------------------" << endl;
    for (int i = history->size() - 1; i >= 0; i--) {
        cout << setw(6) << history->at(i)->action << setw(40) << history->at(i)->movie->getTitle() << "" << endl;// << " " << history[i]->action << endl;
        
    }
}

// string representation of customer
string Customer::toString() const {
    string out = string("");
    out += string(getName());
    out += string(" ");
    out += string(to_string(this->get_id()));
    //out += string("movies out ");
   // out += string(to_string(this->moviesOut()));
    return string(out);
}

// return customers name as a string
string Customer::getName() const{
	return string(name);
}

// return customers id as an int
int Customer::get_id() const {
	return int(id);
}

// constructor stores values on stack so dont need to manually delete
Customer::Customer(string name, int id):history() {
	this->name = string(name);
	this->history = new vector<Transaction*> ();
	this->id = int(id);

}

// destructor
Customer::~Customer() {
    //cout << "destructor for customer called" << endl;
    // manual deletion for transactions stored in the history vector
    //for (size_t i = 0; i < history.size(); i++) {
        //delete history[i];
       // history[i]=0;
    //}
}

