//----------------------Customer.cpp------------------------------
// -
// Matthew Stibbins
// 3/14/2018
// ---------------------------------------------------------------
//
//

#pragma once
#include <string>
#include <iostream>
#include <utility>
#include <vector>
#include <iomanip>
#include "Movie.h"
#include "Transaction.h"

using namespace std;

/**
 * Class: Customer
 *
 * Description: Stores customers information and
 * transaction history
 *
 * Data:
 * 	 string --------------[name]	: static
 *   int -----------------[id] 	 	: static
 * 	 vector<Transaction*> [history] : static : dynamic contents
 * 	 
 */

class Customer {
private:
	string name;
	int id;
public:
    // customer getters
    //friend operator==(Customer& c1, const Customer& c2);
    
    // stores customers history of transactions
    vector<Transaction*>* history;

    // print out the customers transaction history in
    // chronological order (most recent first)
    void printHistory() const;

    // ostream overload adds string representation of customer to the stream
    friend ostream& operator<<(ostream& out, Customer& m);

    // returns the string representation of the customer
    string toString() const;

    // returns the customers name as string
	string getName() const;

    // returns the customers id as int
	int get_id() const;

    // constuctor for customer : stores data on stack
    // transactions in vector stored dynamically and must be 
    // manually deleted
	Customer(string name, int id);
	
    // destructor, manually deletes transactions stored in the history vector
    virtual ~Customer();
};
